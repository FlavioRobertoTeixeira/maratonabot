﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using System;

namespace Bot_Application1.Formularios
{
    [Serializable]
    [Template(TemplateUsage.NotUnderstood,"Desculpe, não entendi \"{0}\".")]
    public class Pedido
    {
        public Pizzas Pizzas { get; set; }
        public Bebidas Bebidas { get; set; }
        public FormaPagamento FormaPagamento { get; set; }
        public Entrega Entrega { get; set; }

        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string Endereco { get; set; }
       
        //Método responsável por construir um formulário do tipo pedido
        public static IForm<Pedido> BuildForm()
        {
            var form = new FormBuilder<Pedido>();
            //Define o padrão de apresentação com a priorização para botões
            form.Configuration.DefaultPrompt.ChoiceStyle = ChoiceStyleOptions.Buttons;
            //Configurando as opções de resposta de confirmação
            form.Configuration.Yes = new string[] { "Sim","y","sim", "s", "ss", "yep" };
            form.Configuration.No = new string[] { "Não","não", "nao", "n", "no" };
            //define a mensagem de resposta quando o usuário entrar neste fluxo de conversa
            form.Message("Olá, seja bem vindo! Será um prazer atender você");
            //Ao completar o fluxo realiza uma ação via Delegate
            form.OnCompletion(async(contexto,pedido)=> {
                //Executa
                await contexto.PostAsync("Seu pedido número 123 foi realizado! Logo, Logo estará ai. ");
            });
            return form.Build();
        }
        
    }

    [Describe("Pizzas")]
    public enum Pizzas {
        [Terms("Calabrsa","Calabres","Cala")]
        [Describe("Calabresa")]
        Calabresa = 1,

        [Terms("estrogonofe", "strogonof", "strogo")]
        [Describe("Strogonoff de Frango")]
        StrogonoffDeFrango,

        [Terms("4 queijos", "quatro queij", "qtr queij")]
        [Describe("Quatro Queijos")]
        QuatroQueijos,
        [Describe("Napolitana")]
        Napolitana
    }

    [Describe("Bebidas")]
    public enum Bebidas
    {
        [Describe("Água")]
        Agua = 1,
        [Describe("Refrigerante")]
        Refrigerante,
        [Describe("Suco")]
        Suco
    }

    [Describe("Forma de Pagamento")]
    public enum FormaPagamento
    {
        [Terms("credito","cre")]
        [Describe("Cartão de Crédito")]
        CartaoCredito = 1,

        [Terms("Debito,deb")]
        [Describe("Cartão de Débito")]
        CartaoDebito,

        [Describe("Dinheiro")]
        Dinheiro
    }

    [Describe("Tipo de Entrega")]
    public enum Entrega
    {
        [Terms("ai,local")]
        [Describe("Retirar no Local")]
        RetirarNoLocal = 1,

        [Terms("moto,motoca")]
        [Describe("Motoboy")]
        Motoboy
    }


}
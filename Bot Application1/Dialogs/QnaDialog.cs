﻿using Microsoft.Bot.Builder.CognitiveServices.QnAMaker;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Threading.Tasks;

namespace Bot_Application1.Dialogs
{
    [Serializable]
    public class QnaDialog : QnAMakerDialog
    {

        private static string QnaSubscriptionKey = ConfigurationManager.AppSettings["QnASubscriptionKey"];
        private static string KnowledgebaseID = ConfigurationManager.AppSettings["QnaKnowledgebaseID"];
        private static string DefaultMessage = "Desculpe, não entendi.";

        public QnaDialog() : base(new QnAMakerService
            (new QnAMakerAttribute(QnaSubscriptionKey, KnowledgebaseID, DefaultMessage, 0.5)))
        {

        }

        protected override async Task RespondFromQnAMakerResultAsync(IDialogContext context, IMessageActivity message, QnAMakerResults result)
        {
            //recupera a primeira resposta
            var primeiraResposta = result.Answers.First().Answer;
            Activity resposta = ((Activity)context.Activity).CreateReply();

            var dadosResposta = primeiraResposta.Split(';');
            if(dadosResposta.Length == 1)
            {
                //caso a resposta seja de tamanho 1 aguarde as proximas e retorne
                await context.PostAsync(primeiraResposta);
                return;
            }

            //recupera o conteudo da mensagem
            var titulo  = dadosResposta[0];
            var descricao = dadosResposta[1];
            var urlImagem = dadosResposta[2];
            var urlRedirecionamento = dadosResposta[3];

            HeroCard card = new HeroCard
            {
                Title = titulo,
                Subtitle = descricao
            };

            card.Buttons = new List<CardAction>
            {
                new CardAction(ActionTypes.OpenUrl,"Compre agora!",urlRedirecionamento)
            };

            card.Images = new List<CardImage>
            {
                new CardImage(url:urlImagem)
            };

            resposta.Attachments.Add(card.ToAttachment());
            await context.PostAsync(resposta);
            
        }


    }
}